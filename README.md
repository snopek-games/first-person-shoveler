# First Person Shoveler!

Our submission for the [Godot Wild Jam #45](https://itch.io/jam/godot-wild-jam-45)!

You can [play it here](https://dsnopek.itch.io/first-person-shoveler).

This was built using Godot 3.4.4.

Since this is a game jam game, the code isn't the greatest, but we hope it will help anyone who may be wondering "how did they do that?" :-)

## Team

- [Logan Dalke](https://twitter.com/LoganMakesGames/) - Art and Design
- [Logan Lang](https://twitter.com/DevLogLogan/) - Programming and Design
- [David Snopek](https://www.snopekgames.com/) - Programming and Design
- [Jamphibious](https://jamphibious.com/) - Music

## License

Copyright 2022 Logan Dalke, Logan Lang, David Snopek, Jordan Michael Reed

- The code (and other Godot engine files) are licensed under the [MIT License](https://gitlab.com/snopek-games/first-person-shoveler/-/blob/main/LICENSE.txt).
- The original art work (contained in the *.glb files in the [Environment/](https://gitlab.com/snopek-games/first-person-shoveler/-/tree/main/Environment) and [Puzzle/](https://gitlab.com/snopek-games/first-person-shoveler/-/tree/main/Puzzle) directories) is licensed under the [CC BY-NC-ND 4.0 License](https://gitlab.com/snopek-games/first-person-shoveler/-/blob/main/LICENSE-ART-AND-MUSIC.txt).
- The original music (contained in the *.ogg files in the [Music/](https://gitlab.com/snopek-games/first-person-shoveler/-/tree/main/Music) directory) is also licensed under the [CC BY-NC-ND 4.0 License](https://gitlab.com/snopek-games/first-person-shoveler/-/blob/main/LICENSE-ART-AND-MUSIC.txt).
- The remaining art and sound assets were acquired from third parties and are distributed in accordance with their individual licenses (see the links below).

### Third Party Assets

- [Jungle Fever Font](https://www.fontsquirrel.com/fonts/JungleFever?q%5Bterm%5D=JungleFever&q%5Bsearch_check%5D=Y) by Nick's Fonts

- [Shovel model](https://sketchfab.com/3d-models/stylized-shovel-86922c4c69244eccba28149c32c7dbd0) by Izzy

- [Shovel dig sound](https://opengameart.org/content/shovel-sound) by themightyglider

- [Shovel miss sound](https://opengameart.org/content/swishes-sound-pack) by artisticdude

- [Shovel wall sound](https://opengameart.org/content/100-cc0-sfx) by rubberduck

- [Footsteps](https://opengameart.org/content/sound-effects-pack) by OwlishMedia

- [UI Click Sound](https://www.kenney.nl/assets/ui-audio) by Kenney

- [Chest shake sound](https://freesound.org/people/zimbot/sounds/89390/) by zimbot

- [Chest thud sound](https://freesound.org/people/newagesoup/sounds/339361/) by newagesoup

- [Chest unlock sound](https://freesound.org/people/IPaddeh/sounds/422852/) by IPaddeh

- [Chest hinge sound](https://freesound.org/people/scholzi982/sounds/566174/) by scholzi982

- [Treasure collect sound](https://freesound.org/people/Scrampunk/sounds/345297/) by Scrampunk

- [Victory sound](https://freesound.org/people/misjoc/sounds/400586/) by misjoc

- [Failure sound](https://freesound.org/people/FunWithSound/sounds/456964/) by FunWithSound

- [Temple button sound](https://opengameart.org/content/stone-rock-or-wood-moved-sound) by pauliuw

- [Temple gate sound](https://opengameart.org/content/cave-in) by StarNinjas
