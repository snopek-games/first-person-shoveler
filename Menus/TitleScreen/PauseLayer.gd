extends CanvasLayer

signal quit

onready var panel = $Panel
onready var click_sound = $ClickSound

var is_paused := false

func _ready():
	panel.visible = false

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed('ui_cancel') and not GameState.game_finished:
		toggle()

func toggle() -> void:
	is_paused = !is_paused
	panel.visible = is_paused
	get_tree().paused = is_paused
	if is_paused:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		lower_volume()
	elif GameState.is_explore_mode or !GameState.puzzle_interface.puzzle_showing:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		raise_volume()

func lower_volume() -> void:
	var index = AudioServer.get_bus_index("Master")
	AudioServer.set_bus_volume_db(index, -12)

func raise_volume() -> void:
	var index = AudioServer.get_bus_index("Master")
	AudioServer.set_bus_volume_db(index, 0)

func _on_Resume_pressed():
	toggle()
	click_sound.play()

func _on_Quit_pressed():
	toggle()
	emit_signal("quit")
	click_sound.play()
