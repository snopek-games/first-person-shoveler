extends CanvasLayer

signal play ()
signal tutorial ()
signal explore ()

onready var main = $Main
onready var settings = $Settings
onready var credits = $Credits
onready var tutorial_button = $Main/VBoxContainer/Tutorial
onready var music_volume = $Settings/VBoxContainer/HBoxContainer/MusicVolume
onready var sfx_volume = $Settings/VBoxContainer/HBoxContainer2/SfxVolume
onready var fullscreen_toggle = $Settings/VBoxContainer/HBoxContainer3/CenterContainer/CheckBox
onready var click_sound = $ClickSound
onready var exit_button = $Main/Exit

var music_bus = AudioServer.get_bus_index("Music")
var sfx_bus = AudioServer.get_bus_index("Sound")

func _ready():
	music_volume.value = AudioServer.get_bus_volume_db(music_bus)
	sfx_volume.value = AudioServer.get_bus_volume_db(sfx_bus)
	fullscreen_toggle.pressed = OS.window_fullscreen

	if OS.has_feature('HTML5'):
		exit_button.hide()

	GameState.load_config()
	if not GameState.player_has_done_tutorial:
		tutorial_button.visible = false
		# Resize to accomidate the missing button.
		# Not enough time to do this the right way!
		main.rect_size.y = 370

func _on_Play_pressed():
	emit_signal("play")
	click_sound.play()

func _on_Tutorial_pressed() -> void:
	emit_signal("tutorial")
	click_sound.play()

func _on_Settings_pressed():
	main.hide()
	settings.show()
	click_sound.play()

func _on_Credits_pressed():
	main.hide()
	credits.show()
	click_sound.play()

func _on_MusicVolume_value_changed(value):
	AudioServer.set_bus_volume_db(music_bus, value)

	if value == -30:
		AudioServer.set_bus_mute(music_bus, true)
	else:
		AudioServer.set_bus_mute(music_bus, false)

func _on_SfxVolume_value_changed(value):
	AudioServer.set_bus_volume_db(sfx_bus, value)

	if value == -30:
		AudioServer.set_bus_mute(sfx_bus, true)
	else:
		AudioServer.set_bus_mute(sfx_bus, false)

func _on_CheckBox_toggled(button_pressed):
	OS.window_fullscreen = button_pressed
	click_sound.play()

func _on_Done_pressed():
	settings.hide()
	main.show()
	click_sound.play()

func _on_RichTextLabel_meta_clicked(meta):
	OS.shell_open(str(meta))

func _on_Back_pressed():
	credits.hide()
	main.show()
	click_sound.play()

func _on_Exit_pressed():
	get_tree().quit()

func _on_Explore_pressed():
	emit_signal("explore")
	click_sound.play()
