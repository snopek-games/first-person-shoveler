extends Spatial

onready var menu_layer := $MenuLayer
onready var screen_transition := $ScreenTransition
onready var camera_pivot := $CameraPivot
onready var title_music = $TitleMusic

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	menu_layer.connect("play", self, "on_play")
	menu_layer.connect("tutorial", self, "on_tutorial")
	menu_layer.connect("explore", self, "on_explore")

func _process(delta):
	camera_pivot.rotation_degrees.y += 10 * delta

func on_play() -> void:
	if GameState.player_has_done_tutorial:
		screen_transition.transition()
	else:
		on_tutorial()

func on_tutorial() -> void:
	screen_transition.transition("res://Tutorial/Tutorial.tscn")

func on_explore() -> void:
	screen_transition.transition("res://ExploreMode/ExploreMode.tscn")
