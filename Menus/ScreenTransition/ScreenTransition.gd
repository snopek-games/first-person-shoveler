extends CanvasLayer

export (String, FILE, "*.tscn") var target_scene
export (float) var fade_duration := 0.5

onready var color_rect := $ColorRect
onready var tween := $Tween
onready var music_tween := $MusicTween
onready var timer := $Timer

func _ready() -> void:
	color_rect.visible = true
	tween.interpolate_property(color_rect, "modulate:a", 1, 0, fade_duration, 0, Tween.EASE_OUT)
	tween.interpolate_callback(color_rect, fade_duration, "hide")
	music_tween.interpolate_method(self, "change_master_bus_volume", -30.0, 0.0, fade_duration)
	
	tween.start()
	music_tween.start()

func change_master_bus_volume(value: float):
	var index = AudioServer.get_bus_index("Master")
	AudioServer.set_bus_volume_db(index, value)

func transition(next_scene := target_scene) -> void:
	color_rect.show()
	tween.interpolate_property(color_rect, "modulate:a", 0, 1, fade_duration, 0 , Tween.EASE_IN)
	music_tween.interpolate_method(self, "change_master_bus_volume", 0.0, -30.0, fade_duration)
	tween.start()
	music_tween.start()
	yield(tween, "tween_all_completed")
	target_scene = next_scene
	timer.start()

func _on_Timer_timeout():
	get_tree().change_scene(target_scene)
