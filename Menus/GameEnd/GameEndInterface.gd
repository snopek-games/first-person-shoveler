extends CanvasLayer

signal play_again
signal quit

onready var lose_display = $TextureRect/LoseDisplay
onready var loss_label = $TextureRect/LoseDisplay/VBoxContainer/LossLabel
onready var win_display = $TextureRect/WinDisplay
onready var win_label = $TextureRect/WinDisplay/VBoxContainer/WinLabel
onready var anim_player = $AnimationPlayer
onready var failure_sound = $FailureSound
onready var victory_sound = $VictorySound

func display_loss(treasures_remaining: int) -> void:
	failure_sound.play()
	loss_label.text = "there were still %s\ntreasures remaining!" % str(treasures_remaining)
	lose_display.show()
	anim_player.play("slide_in")

func display_win(time_elapsed: float) -> void:
	victory_sound.play()
	var minutes = int(time_elapsed) / 60
	var seconds = int(time_elapsed) % 60
	win_label.text = "you found all of the\ntreasure in %1d:%02d!" % [minutes, seconds]
	win_display.show()
	anim_player.play("slide_in")
	
	if lose_display.visible:
		lose_display.hide()
		failure_sound.stop()
		win_label.text = "you found all of the\ntreasure in 4:00!"

func _on_TryAgain_pressed():
	emit_signal("play_again")

func _on_Quit_pressed():
	emit_signal("quit")

func _on_AnimationPlayer_animation_finished(anim_name):
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
