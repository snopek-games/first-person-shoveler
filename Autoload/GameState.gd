extends Node

const USER_CONFIG_FILE := "user://config.ini"

var player
var puzzle_interface

var chests_remaining
var game_finished = false
var is_explore_mode = false

var player_has_done_tutorial := false setget set_player_has_done_tutorial

func setup_game_state(_player, _puzzle_interface) -> void:
	player = _player
	puzzle_interface = _puzzle_interface

func load_config() -> void:
	var file := ConfigFile.new()
	if file.load(USER_CONFIG_FILE) == OK:
		player_has_done_tutorial = file.get_value('tutorial', 'player_has_done_tutorial', false)

func save_config() -> void:
	var file := ConfigFile.new()
	file.set_value("tutorial", "player_has_done_tutorial", player_has_done_tutorial)
	file.save(USER_CONFIG_FILE)

func set_player_has_done_tutorial(_player_has_done_tutorial) -> void:
	if player_has_done_tutorial != _player_has_done_tutorial:
		player_has_done_tutorial = _player_has_done_tutorial
		save_config()
