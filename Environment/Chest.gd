extends Spatial

onready var rock_pile = $RockPile
onready var treasure = $TreasureContainer/Treasure

signal chest_hit ()

func interact() -> void:
	rock_pile.visible = false
	yield(get_tree().create_timer(0.1), "timeout")
	treasure.show()
	treasure.place()
	yield(get_tree().create_timer(0.1), "timeout")
	emit_signal("chest_hit")
