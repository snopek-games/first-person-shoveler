extends Area

const MAX_SPAWN_ATTEMPTS := 5

onready var collision_shape: CollisionShape = $CollisionShape

var spawns: Spatial

func _ready() -> void:
	spawns = Spatial.new()
	spawns.name = "Spawns"
	add_child(spawns)

func _get_bounds() -> AABB:
	if not collision_shape.shape is BoxShape:
		return AABB()

	var box_shape: BoxShape = collision_shape.shape
	var center = collision_shape.translation

	return AABB(center - box_shape.extents, box_shape.extents * 2.0)

func _has_existing_spawn(test_position: Vector3, margin: float) -> bool:
	for child in spawns.get_children():
		var distance = child.translation.distance_to(test_position)
		if distance <= margin:
			return true
	return false

func spawn(scene: PackedScene, margin: float) -> Spatial:
	var bounds := _get_bounds()
	if bounds.size == Vector3.ZERO:
		return null

	var attempt = MAX_SPAWN_ATTEMPTS
	while attempt > 0:
		var proposed_position := Vector3(
			bounds.position.x + randf() * bounds.size.x,
			collision_shape.translation.y,
			bounds.position.z + randf() * bounds.size.z)

		if not _has_existing_spawn(proposed_position, margin):
			var spawn: Spatial = scene.instance()
			spawns.add_child(spawn)
			spawn.transform = collision_shape.transform
			spawn.translation = proposed_position
			return spawn

		attempt -= 1

	return null
