extends Node

const Chest: PackedScene = preload("res://Environment/Chest.tscn")

signal update_remaining_treasure

const NUM_CHESTS := 10

onready var chest_spawn_areas = $ChestSpawnAreas

var selected_chest = null

func setup_world() -> void:
	GameState.puzzle_interface.connect("puzzle_over", self, "on_puzzle_over")
	GameState.chests_remaining = NUM_CHESTS
	_setup_chests()


func _setup_chests() -> void:
	var spawn_areas = chest_spawn_areas.get_children()
	spawn_areas.shuffle()

	for i in NUM_CHESTS:
		var chest = spawn_areas[i].spawn(Chest, 1.0)
		if chest:
			chest.connect("chest_hit", self, "on_chest_hit", [chest])

func on_chest_hit(chest_ref):
	selected_chest = chest_ref

func on_puzzle_over():
	if selected_chest:
		selected_chest.treasure.open()
		selected_chest = null
	yield(get_tree().create_timer(0.5), "timeout")
	emit_signal("update_remaining_treasure")
