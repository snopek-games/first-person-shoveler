extends Area

onready var animation_player: AnimationPlayer = $AnimationPlayer
onready var tween: Tween = $Tween
onready var button_sound: AudioStreamPlayer = $ButtonSound
onready var door_sound: AudioStreamPlayer = $DoorSound

export (NodePath) var door_path: NodePath

var door: Spatial

func _ready() -> void:
	door = get_node(door_path)

func interact() -> void:
	animation_player.play("Press")
	button_sound.play()

func _on_DoorButton_body_entered(body: Node) -> void:
	interact()

func _on_AnimationPlayer_animation_finished(anim_name:String) -> void:
	if anim_name == "Press":
		tween.interpolate_property(door, "translation:y", door.translation.y, -8.2, 3.0)
		tween.start()
		door_sound.play()
