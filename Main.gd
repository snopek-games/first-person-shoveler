extends Spatial

onready var player = $Player
onready var puzzle_interface = $PuzzleInterface
onready var pause_layer = $PauseLayer
onready var game_end_interface = $GameEndInterface
onready var hud = $HUD
onready var screen_transition = $ScreenTransition
onready var world = $World
onready var stage_music = $StageMusic
onready var tween = $Tween

func _ready() -> void:
	GameState.is_explore_mode = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	GameState.game_finished = false
	randomize()

	pause_layer.connect("quit", self, "on_quit")
	hud.connect("time_limit_reached", self, "on_time_limit_reached")
	world.connect("update_remaining_treasure", self, "on_update_remaining_treasure")
	game_end_interface.connect("quit", self, "on_quit")
	game_end_interface.connect("play_again", self, "on_play_again")

	GameState.setup_game_state(player, puzzle_interface)
	world.setup_world()

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("restart") and OS.has_feature('debug'):
		get_tree().reload_current_scene()

	if event is InputEventMouseButton:
		if event.pressed:
			if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
				get_tree().set_input_as_handled()

func on_quit() -> void:
	GameState.game_finished = true
	screen_transition.transition()

func on_play_again() -> void:
	screen_transition.transition(filename)

func on_time_limit_reached() -> void:
	if GameState.puzzle_interface.puzzle_showing:
		GameState.puzzle_interface.force_close()
	
	lower_music_volume()
	var treasures_left = GameState.chests_remaining
	game_end_interface.display_loss(treasures_left)
	GameState.game_finished = true

func on_update_remaining_treasure() -> void:
	var treasures_left = GameState.chests_remaining - 1
	GameState.chests_remaining = treasures_left
	hud.update_treasure_remaining(treasures_left)
	if (treasures_left == 0):
		hud.fade_out_hud()
		lower_music_volume()
		game_end_interface.display_win(hud.timer.wait_time - hud.timer.time_left)
		GameState.game_finished = true

func lower_music_volume() -> void:
	tween.interpolate_property(stage_music, "volume_db", stage_music.volume_db, stage_music.volume_db - 14.0, 1.0)
	tween.start()
