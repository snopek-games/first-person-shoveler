extends Spatial

onready var anim_player = $AnimationPlayer
onready var drop_sound = $DropSound
onready var open_sound = $OpenSound
onready var collect_sound = $CollectSound

func place() -> void:
	get_parent().look_at(GameState.player.transform.origin, Vector3.UP)
	get_parent().rotate(Vector3.UP, PI)
	get_parent().rotation_degrees.x = 0
	show()
	anim_player.play("place")

func open() -> void:
	anim_player.play("open")
	open_sound.play()

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "place":
		drop_sound.play()
		GameState.puzzle_interface.show_puzzle()
	if anim_name == "open":
		collect_sound.play()
		anim_player.play("shrink")
	if anim_name == "shrink":
		get_parent().get_parent().queue_free()
