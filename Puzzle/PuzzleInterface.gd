extends CanvasLayer

signal puzzle_over

var num_a: int
var num_b: int

var shake_amount = 0.0
var shake_dampening = 0.09

onready var num_a_label = $Panel/Numbers/HBoxContainer/NumALabel
onready var num_b_label = $Panel/Numbers/HBoxContainer/NumBLabel
onready var answer_ten = $Panel/Numbers/AnswerTen
onready var answer_one = $Panel/Numbers/AnswerOne
onready var open_button = $Panel/Open
onready var anim_player = $AnimationPlayer
onready var click_sound = $ClickSound
onready var shake_sound = $ShakeSound
onready var unlock_sound = $UnlockSound

var puzzle_showing := false setget set_puzzle_showing

func _ready():
	randomize()

func _process(delta):
	if !puzzle_showing:
		return
	
	if shake_amount != 0.0:
		offset.x = rand_range(-1, 1) * shake_amount
		offset.y = rand_range(-1, 1) * shake_amount
		shake_amount = lerp(shake_amount, 0.0, shake_dampening)
	else:
		offset = Vector2.ZERO

func show_puzzle():
	num_a = (randi() % 49) + 1
	num_b = (randi() % 49) + 1
	num_a_label.text = str(num_a)
	num_b_label.text = str(num_b)
	answer_ten.text = "0"
	answer_one.text = "0"
	
	anim_player.play("slide_in")
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_TenUp_pressed():
	var answer_plus_ten = int(answer_ten.text) + 1
	answer_ten.text = str(answer_plus_ten % 10)
	click_sound.play()

func _on_OneUp_pressed():
	var answer_plus_one = int(answer_one.text) + 1
	answer_one.text = str(answer_plus_one % 10)
	click_sound.play()

func _on_TenDown_pressed():
	var answer_minus_ten = int(answer_ten.text) - 1
	answer_ten.text = str(int(fposmod(answer_minus_ten, 10.0)))
	click_sound.play()

func _on_OneDown_pressed():
	var answer_minus_one = int(answer_one.text) - 1
	answer_one.text = str(int(fposmod(answer_minus_one, 10.0)))
	click_sound.play()

func _on_Open_pressed():
	var answer = int(answer_ten.text + answer_one.text)
	if answer == num_a + num_b:
		emit_signal("puzzle_over")
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		open_button.release_focus()
		anim_player.play("slide_out")
		unlock_sound.play()
		puzzle_showing = false
	else:
		screen_shake()
		shake_sound.play()

func screen_shake() -> void:
	shake_amount += 10

func force_close() -> void:
	puzzle_showing = false
	anim_player.play("slide_out")

func set_puzzle_showing(val) -> void:
	puzzle_showing = val
	# in case user clicks screen before puzzle fully slides in:
	if puzzle_showing:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
