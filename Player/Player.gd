extends KinematicBody

const Shovel = preload("res://Shovel/Shovel.gd")

export var mouse_sensitivity := 0.002
export var max_speed := 6.0
export var gravity := -30.0
export var jump_speed := 12.0
export var coyote_frames := 5
export var jump_buffer_frames := 5
export var big_fall_threshold := 0.7

var velocity := Vector3.ZERO
var next_step_sound := 0

onready var pivot: Spatial = $Pivot
onready var camera: Camera  = $Pivot/Camera
onready var ground_ray_cast: RayCast = $GroundRayCast
onready var interaction_ray_cast: RayCast = $Pivot/InteractionRayCast
onready var environment_ray_cast: RayCast = $Pivot/EnvironmentRayCast
onready var shovel = $Pivot/Shovel
onready var state_machine = $StateMachine
onready var step_sounds = [ $StepSound1, $StepSound2 ]
onready var big_fall_sound = $BigFallSound
onready var digging_progress_bar = $CanvasLayer/DiggingProgressBar
onready var debug_state_label = $CanvasLayer/DebugStateLabel

enum EnvironmentType {
	NONE,
	GROUND,
	WALL,
}

func _readonly_variable(_value) -> void:
	pass

func _ready() -> void:
	state_machine.change_state("Idle")
	state_machine.connect("state_changed", self, "_on_StateMachine_state_changed")

func _on_StateMachine_state_changed(new_state) -> void:
	debug_state_label.text = "State: %s" % new_state.name

func get_input_vector() -> Vector3:
	var input_dir: Vector3 = Vector3.ZERO
	if Input.is_action_pressed("move_forward"):
		input_dir += -global_transform.basis.z
	if Input.is_action_pressed("move_backward"):
		input_dir += global_transform.basis.z
	if Input.is_action_pressed("move_left"):
		input_dir += -global_transform.basis.x
	if Input.is_action_pressed("move_right"):
		input_dir += global_transform.basis.x
	input_dir = input_dir.normalized()
	return input_dir

func apply_gravity(delta: float) -> void:
	velocity.y += gravity * delta

func move(is_jumping: bool = false) -> void:
	if is_jumping:
		velocity = move_and_slide(velocity, Vector3.UP, true)
	else:
		velocity = move_and_slide_with_snap(velocity, Vector3(0, -0.3, 0), Vector3.UP, true)

func check_environment_raycast() -> int:
	environment_ray_cast.force_raycast_update()
	if environment_ray_cast.is_colliding():
		var collision_normal: Vector3 = environment_ray_cast.get_collision_normal()
		var angle = abs(Vector3.UP.angle_to(collision_normal))
		if angle <= deg2rad(15):
			return EnvironmentType.GROUND
		else:
			return EnvironmentType.WALL
	return EnvironmentType.NONE

# Ensures we don't go into the falling state if we just get a teeny tiny
# bit of air from bumping into the environment.
func is_really_on_floor() -> bool:
	ground_ray_cast.force_raycast_update()
	return is_on_floor() or ground_ray_cast.is_colliding()

func show_digging_progress(progress: float) -> void:
	digging_progress_bar.visible = true
	digging_progress_bar.progress = progress

func hide_digging_progress() -> void:
	digging_progress_bar.visible = false

func _unhandled_input(event: InputEvent) -> void:
	if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED or (!GameState.is_explore_mode and GameState.puzzle_interface.puzzle_showing):
		return
	if event is InputEventMouseMotion:
		rotate_y(-event.relative.x * mouse_sensitivity)
		pivot.rotate_x(-event.relative.y * mouse_sensitivity)
		pivot.rotation.x = clamp(pivot.rotation.x, -1.2, 1.2)

func _on_Shovel_animation_stepped() -> void:
	step_sounds[next_step_sound].play()
	next_step_sound = 1 if next_step_sound == 0 else 0
