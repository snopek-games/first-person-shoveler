extends CanvasLayer

export (int) var time_limit_seconds = 300

signal time_limit_reached

onready var timer := $Timer
onready var labels := $Labels
onready var time_remaining := $Labels/TimeRemaining
onready var treasures_remaining := $Labels/TreasureRemaining
onready var anim_player = $AnimationPlayer

func _ready():
	timer.wait_time = time_limit_seconds
	timer.start()

func _process(delta):
	update_time_remaining(timer.time_left)

func update_time_remaining(time_left) -> void:
	var minutes := (int(time_left) / 60)
	var seconds := (int(time_left) % 60)
	time_remaining.text = "%1d:%02d" % [minutes, seconds]

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("toggle_hud"):
		labels.visible = not labels.visible

func update_treasure_remaining(amount_left) -> void:
	treasures_remaining.text = "%s Remaining" % str(amount_left)

func fade_out_hud() -> void:
	timer.paused = true
	anim_player.play("fade_out")

func _on_Timer_timeout():
	emit_signal("time_limit_reached")
	set_process(false)
	fade_out_hud()
