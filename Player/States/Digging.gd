extends "res://addons/snopek-state-machine/State.gd"

const Player = preload("res://Player/Player.gd")
const Shovel = preload("res://Shovel/Shovel.gd")
const DigDirtParticles = preload("res://Shovel/DigDirtParticles.tscn")
const PokeDirtParticles = preload("res://Shovel/PokeDirtParticles.tscn")

onready var host = $"../.."
onready var digging_timer = $DiggingTimer
onready var dig_sound = $DigSound
onready var poke_sound = $PokeSound

var skipped_first_dig_start := false
var digging_target = null

func _ready() -> void:
	host.connect("ready", self, "_on_host_ready")
	digging_timer.connect("timeout", self, "_on_DiggingTimer_timeout")

func _on_host_ready() -> void:
	host.shovel.connect("animation_dig_start", self, "_on_shovel_animation_dig_start")
	host.shovel.connect("animation_dig_toss", self, "_on_shovel_animation_dig_toss")

func _state_enter(info: Dictionary) -> void:
	host.shovel.state = Shovel.State.DIG
	skipped_first_dig_start = false
	digging_target = info.get("digging_target", null)
	digging_timer.start()
	host.show_digging_progress(0)

func _state_exit() -> void:
	digging_timer.stop()
	host.hide_digging_progress()

func _state_physics_process(delta: float) -> void:
	if not Input.is_action_pressed("interact"):
		get_parent().change_state("Idle")
		return

	check_digging_target()

func _state_process(delta: float) -> void:
	host.show_digging_progress(clamp(1.0 - (digging_timer.time_left / digging_timer.wait_time), 0.0, 1.0))

func check_digging_target() -> void:
	var new_digging_target: Spatial

	host.interaction_ray_cast.force_raycast_update()
	if host.interaction_ray_cast.is_colliding():
		new_digging_target = host.interaction_ray_cast.get_collider()

		# If we point at an interactable item that's not diggable,
		# just stop.
		if not new_digging_target.is_in_group("diggable"):
			get_parent().change_state("Idle")
			return

	# If we're not pointing at a valid digging target, then...
	if new_digging_target == null:
		match host.check_environment_raycast():
			Player.EnvironmentType.GROUND:
				# Restart timer if we used to be digging a valid target.
				# But otherwise we keep on digging!
				if digging_target != null:
					digging_target = null
					digging_timer.start()

			Player.EnvironmentType.WALL, Player.EnvironmentType.NONE:
				# Nothing to dig here, move on!
				get_parent().change_state("Idle")
		return

	# If we switch to a valid digging target that's different, then...
	if new_digging_target != null and new_digging_target != digging_target:
		digging_target = new_digging_target
		digging_timer.start()
		return

func _on_shovel_animation_dig_start() -> void:
	if not skipped_first_dig_start:
		skipped_first_dig_start = true
		return

	poke_sound.play()
	host.shovel.spawn_particles(PokeDirtParticles)

func _on_shovel_animation_dig_toss() -> void:
	dig_sound.play()
	host.shovel.spawn_particles(DigDirtParticles)

func _on_DiggingTimer_timeout() -> void:
	if digging_target:
		digging_target.interact()

	get_parent().change_state("Idle")
