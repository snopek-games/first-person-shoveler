extends "res://Player/States/Walking.gd"

onready var jump_sounds = [ $JumpSound1, $JumpSound2, $JumpSound3, $JumpSound4 ]

const MAX_JUMP_FRAMES := 25

var jump_frames := 0
var next_jump_sound := 0

func _state_enter(info: Dictionary) -> void:
	host.shovel.state = Shovel.State.JUMP
	host.velocity.y = host.jump_speed
	if info.has('input_vector'):
		do_move(info['input_vector'], true, false)

	jump_frames = 0

	if not host.big_fall_sound.playing:
		jump_sounds[next_jump_sound].play()
		var last_jump_sound = next_jump_sound
		while next_jump_sound == last_jump_sound:
			next_jump_sound = randi() % jump_sounds.size()

func _state_physics_process(delta: float) -> void:
	# Don't let a jump last too long!
	if jump_frames >= MAX_JUMP_FRAMES:
		host.velocity.y = 0
		get_parent().change_state("Falling")
		return
	jump_frames += 1

	var old_velocity_y = host.velocity.y
	do_move(host.get_input_vector())

	# Try to prevent climbing things in the jumping state.
	host.velocity.y = min(host.velocity.y, old_velocity_y)

	if host.is_really_on_floor():
		get_parent().change_state("Idle", {landing = true})
		return

	# Allow cancelling the jump!
	if host.velocity.y > 0 and Input.is_action_just_released("jump"):
		host.velocity.y = 0

	if host.velocity.y <= 0:
		get_parent().change_state("Falling")
		return
