extends "res://Player/States/Walking.gd"

var fall_start_time := 0
var coyote_frames_left := 0
var jump_buffer_frames_left := 0

func _state_enter(info: Dictionary) -> void:
	host.shovel.state = Shovel.State.FALL
	fall_start_time = OS.get_ticks_msec()
	if info.get('coyote_time', false):
		coyote_frames_left = host.coyote_frames
	else:
		coyote_frames_left = 0
	jump_buffer_frames_left = 0

func _state_physics_process(delta: float) -> void:
	var input_vector = host.get_input_vector()

	var is_jump_pressed = Input.is_action_just_pressed("jump")

	if coyote_frames_left > 0:
		coyote_frames_left -= 1
		if is_jump_pressed:
			get_parent().change_state("Jumping", {input_vector = input_vector})
			return

	if is_jump_pressed:
		jump_buffer_frames_left = host.jump_buffer_frames

	var old_y = host.translation.y
	do_move(input_vector)

	# Try to prevent going up in the falling state.
	host.translation.y = min(host.translation.y, old_y)

	if host.is_really_on_floor():
		var fall_length: float = (OS.get_ticks_msec() - fall_start_time) / 1000.0
		if fall_length >= host.big_fall_threshold:
			host.big_fall_sound.play()
			# Don't let them buffer a jump from a big fall
			jump_buffer_frames_left = 0

		if jump_buffer_frames_left > 0:
			get_parent().change_state("Jumping")
		elif input_vector != Vector3.ZERO:
			get_parent().change_state("Walking", {landing = true})
		else:
			get_parent().change_state("Idle", {landing = true})
		return

	if jump_buffer_frames_left > 0:
		jump_buffer_frames_left -= 1
