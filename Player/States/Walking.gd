extends "res://Player/States/Idle.gd"

func _state_enter(info: Dictionary) -> void:
	if info.get('landing', false):
		host.shovel.state = Shovel.State.LAND_TO_WALK
	else:
		host.shovel.state = Shovel.State.WALK
	if info.has('input_vector'):
		do_move(info['input_vector'])

func do_move(input_vector: Vector3, is_jumping: bool = false, apply_gravity: bool = true) -> void:
	var delta = get_physics_process_delta_time()
	if apply_gravity:
		host.apply_gravity(delta)

	var desired_velocity: Vector3 = input_vector * host.max_speed
	host.velocity.x = desired_velocity.x
	host.velocity.z = desired_velocity.z
	host.move(is_jumping)

func _state_physics_process(delta: float) -> void:
	if GameState.game_finished:
		get_parent().change_state("Idle")
		return

	if Input.is_action_just_pressed("interact"):
		get_parent().change_state("Poking")
		return

	var input_vector = host.get_input_vector()

	if Input.is_action_just_pressed("jump") and host.is_really_on_floor():
		get_parent().change_state("Jumping", {input_vector = input_vector})
		return

	do_move(input_vector)

	if not host.is_really_on_floor() and host.velocity.y < 0:
		get_parent().change_state("Falling", {coyote_time = true})
		return

	if input_vector == Vector3.ZERO:
		get_parent().change_state("Idle")
		return


