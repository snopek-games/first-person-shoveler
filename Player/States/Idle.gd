extends "res://addons/snopek-state-machine/State.gd"

const Shovel = preload("res://Shovel/Shovel.gd")

onready var host = $"../.."

func _state_enter(info: Dictionary) -> void:
	host.velocity = Vector3.ZERO
	if info.get('landing', false):
		host.shovel.state = Shovel.State.LAND_TO_IDLE
	else:
		host.shovel.state = Shovel.State.IDLE

func _state_physics_process(delta: float) -> void:
	if GameState.puzzle_interface.puzzle_showing or GameState.game_finished:
		return

	if Input.is_action_just_pressed("interact"):
		get_parent().change_state("Poking")
		return

	var input_vector = host.get_input_vector()

	if Input.is_action_just_pressed("jump") and host.is_really_on_floor():
		get_parent().change_state("Jumping", {input_vector = input_vector})
		return

	if input_vector != Vector3.ZERO:
		get_parent().change_state("Walking", {input_vector = input_vector})
		return
