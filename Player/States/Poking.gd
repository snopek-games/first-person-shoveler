extends "res://addons/snopek-state-machine/State.gd"

const Player = preload("res://Player/Player.gd")
const Shovel = preload("res://Shovel/Shovel.gd")
const PokeDirtParticles = preload("res://Shovel/PokeDirtParticles.tscn")
const PokeWallParticles = preload("res://Shovel/PokeWallParticles.tscn")

onready var host = $"../.."
onready var poking_timer = $PokingTimer
onready var poke_dirt_sound = $PokeDirtSound
onready var poke_wall_sound = $PokeWallSound
onready var poke_air_sound = $PokeAirSound

enum InteractionType {
	NONE,
	DIG,
	ACTIVATE,
}

var digging := false
var digging_target: Spatial = null

func _ready() -> void:
	host.connect("ready", self, "_on_host_ready")
	poking_timer.connect("timeout", self, "_on_PokingTimer_timeout")

func _on_host_ready() -> void:
	host.shovel.connect("animation_poked", self, "_on_shovel_animation_poked")

func _state_enter(info: Dictionary) -> void:
	host.shovel.state = Shovel.State.POKE
	host.velocity = Vector3.ZERO
	digging = false
	digging_target = null
	poking_timer.start()

func _state_exit() -> void:
	poking_timer.stop()

func _state_physics_process(delta: float) -> void:
	if not Input.is_action_pressed("interact"):
		get_parent().change_state("Idle")
		return

func _on_shovel_animation_poked() -> void:
	if get_parent().current_state == self:
		host.interaction_ray_cast.force_raycast_update()
		if host.interaction_ray_cast.is_colliding():
			digging_target = host.interaction_ray_cast.get_collider()
			if digging_target.has_method("interact"):
				if digging_target.is_in_group("diggable"):
					digging = true
					poke_dirt_sound.play()
					host.shovel.spawn_particles(PokeDirtParticles)
				else:
					digging_target.interact()
					digging_target = null
					host.shovel.spawn_particles(PokeWallParticles)
					poke_wall_sound.play()
				return

		match host.check_environment_raycast():
			Player.EnvironmentType.GROUND:
				poke_dirt_sound.play()
				host.shovel.spawn_particles(PokeDirtParticles)
				digging = true
				return

			Player.EnvironmentType.WALL:
				host.shovel.spawn_particles(PokeWallParticles)
				poke_wall_sound.play()
				pass

			Player.EnvironmentType.NONE:
				poke_air_sound.play()
				pass

func _on_PokingTimer_timeout() -> void:
	if get_parent().current_state == self:
		if digging:
			get_parent().change_state("Digging", {digging_target = digging_target})
		else:
			get_parent().change_state("Idle")
		return
