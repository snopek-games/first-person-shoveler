extends Control

onready var fill_color: ColorRect = $FillColor

export (float) var progress := 0.0 setget set_progress

func _ready() -> void:
	fill_color.rect_size.x = 0

func set_progress(_progress: float) -> void:
	if progress != _progress:
		progress = _progress
		fill_color.rect_size.x = (rect_size.x - 20) * progress
