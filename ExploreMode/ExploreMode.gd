extends Spatial

onready var player = $Player
onready var puzzle_interface = $PuzzleInterface
onready var world = $World
onready var pause_layer = $PauseLayer
onready var screen_transition = $ScreenTransition

func _ready():
	GameState.game_finished = false
	GameState.is_explore_mode = true
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	pause_layer.connect("quit", self, "on_quit")
	randomize()
	
	GameState.setup_game_state(player, puzzle_interface)
	world.setup_world()

func on_quit() -> void:
	GameState.game_finished = true
	screen_transition.transition()
