extends Spatial

onready var player = $Player
onready var chest = $TutorialWorld/Chest
onready var puzzle_interface = $PuzzleInterface
onready var pause_layer = $PauseLayer
onready var screen_transition = $ScreenTransition
onready var world = $World
onready var stage_music = $StageMusic
onready var animation_player = $AnimationPlayer
onready var gate_sound = $GateSound
onready var label = $CanvasLayer/Label

func _ready() -> void:
	GameState.is_explore_mode = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	pause_layer.connect("quit", self, "on_quit")
	puzzle_interface.connect("puzzle_over", self, "on_puzzle_over")
	chest.connect("chest_hit", self, "on_chest_hit")

	GameState.game_finished = false
	GameState.setup_game_state(player, puzzle_interface)

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("restart") and OS.has_feature('debug'):
		get_tree().reload_current_scene()

	if event is InputEventMouseButton:
		if event.pressed:
			if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
				get_tree().set_input_as_handled()

func on_chest_hit() -> void:
	label.visible = false

func on_puzzle_over() -> void:
	chest.treasure.open()
	yield(get_tree().create_timer(2.0), "timeout")
	animation_player.play("Lower Gate")
	gate_sound.play()

func _on_ExitTutorialArea_body_entered(body:Node) -> void:
	if not GameState.player_has_done_tutorial:
		GameState.player_has_done_tutorial = true
		screen_transition.transition("res://Main.tscn")
	else:
		screen_transition.transition()

func on_quit() -> void:
	GameState.game_finished = true
	screen_transition.transition()

