extends Particles

func _ready() -> void:
	one_shot = true
	emitting = true

func _on_Timer_timeout() -> void:
	queue_free()
