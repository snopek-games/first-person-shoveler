extends Spatial

onready var camera = $ViewportContainer/Viewport/Camera
onready var animation_player = $AnimationPlayer
onready var particle_spawn_point = $Model/ParticleSpawnPoint
onready var crosshair = $CrossHairLayer/Crosshair

enum State {
	IDLE,
	WALK,
	JUMP,
	FALL,
	LAND_TO_IDLE,
	LAND_TO_WALK,
	POKE,
	DIG,
}

var state: int = State.IDLE setget set_state

signal animation_poked ()
signal animation_dig_start ()
signal animation_dig_toss ()
signal animation_stepped ()

func _animation_poked() -> void:
	if state == State.POKE:
		emit_signal("animation_poked")

func _animation_dig_start() -> void:
	emit_signal("animation_dig_start")

func _animation_dig_toss() -> void:
	emit_signal("animation_dig_toss")

func _animation_stepped() -> void:
	emit_signal("animation_stepped")

func set_state(new_state: int) -> void:
	if state != new_state:
		match new_state:
			State.IDLE:
				if state in [State.DIG, State.POKE]:
					animation_player.play_backwards("Poke")
				else:
					animation_player.play("Idle")

			State.WALK:
				animation_player.play("Walk")

			State.JUMP:
				animation_player.play("Jump")

			State.FALL:
				animation_player.play("Fall")

			State.LAND_TO_IDLE, State.LAND_TO_WALK:
				animation_player.play("Land", 0.05)

			State.POKE:
				animation_player.play("Poke")

			State.DIG:
				animation_player.play("Dig")

		state = new_state

func _process(_delta: float) -> void:
	camera.global_transform = global_transform

	var crosshair_visible = Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED
	if crosshair.visible != crosshair_visible:
		crosshair.visible = crosshair_visible

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "Poke":
		if state != State.POKE:
			state = State.IDLE
			animation_player.play("Idle")
	elif anim_name in ["Jump", "Fall", "Land"]:
		if state == State.LAND_TO_WALK:
			state = State.WALK
			animation_player.play("Walk from Land", 0.0)
		else:
			state = State.IDLE
			animation_player.play("Idle")
	elif anim_name == "Walk from Land":
		animation_player.play("Walk", 0.05)

func spawn_particles(scene: PackedScene) -> void:
	var particles: Spatial = scene.instance()
	particles.set_as_toplevel(true)
	add_child(particles)
	particles.transform = particle_spawn_point.global_transform
	#particles.translation = particle_spawn_point.global_transform.origin
